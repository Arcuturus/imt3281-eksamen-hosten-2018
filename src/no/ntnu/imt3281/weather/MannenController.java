package no.ntnu.imt3281.weather;

import javafx.scene.control.Label;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;

public class MannenController extends Label {

    public MannenController() {
        super();
        this.setText(harMannenFalt());
    }

    /**
     * https://jsoup.org/
     * @return Ja or Nei depending on if mannen has fallen
     * @throws IOException
     */
    public String harMannenFalt() {
        Document doc = null;
        try {
            doc = Jsoup.connect("https://www.harmannenfalt.no/").get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Element answer = doc.getElementById("yesnomaybe");
        if(answer.toString().contains("NEI")) return "Mannen har ikke falt.";
        else return "Mannen har falt.";
    }

}
