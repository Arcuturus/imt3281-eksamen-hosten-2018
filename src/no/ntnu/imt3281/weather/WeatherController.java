package no.ntnu.imt3281.weather;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.scene.control.SplitPane;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import no.ntnu.imt3281.yr_db.DBAPI;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public class WeatherController extends Application {

    @FXML private WebView map;
    @FXML private BorderPane bpane;
    private Scene scene;
    DBAPI db;

    public void initialize() throws SQLException {
        db = DBAPI.getMemoryInstance();
        map.getEngine().load("http://folk.ntnu.no/oeivindk/imt3281/map/pseudoMap.html");
        MannenController mannen = new MannenController();
        bpane.setBottom(mannen);
    }

    /**
     * Get mouse x and y position, return closest mathcing place to this position
     * @param event
     */
    @FXML
    void mapClickEvent(MouseEvent event) {
        map.getEngine().setOnAlert(e -> {
            /* Fetch the js alert and get the data from it */
            List<String> pos = Arrays.asList(e.getData().split("\t"));
            try {
                /* Call the findPlaceByPosition function which returns the closest place */
                String place = db.findPlaceByPosition(Double.parseDouble(pos.get(0)), Double.parseDouble(pos.get(1)));
                /* Generate alert based on long, lat and place name */
                generateAlert(pos, place);
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        });
    }

    /**
     * Generic alert function. Shows an alert based on parameters
     * @param cords the longitude and latitude
     * @param name name of the closest place
     */
    private void generateAlert(List<String> cords, String name) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Location");
        alert.setHeaderText(null);
        alert.setContentText(cords.get(0) + " , " + cords.get(1) + ": " + name);
        alert.showAndWait();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        System.out.println("Launching....");
        SplitPane root = FXMLLoader.load(getClass().getResource("Weather.fxml"));
        scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
