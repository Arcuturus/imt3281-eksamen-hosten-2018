package no.ntnu.imt3281.yr_places;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Parser {

    private static URL url;
    private static BufferedReader br;
    private static List<List<String>> steder;

    /**
     * Parses http://fil.nrk.no/yr/viktigestader/noreg.txt and generates a list of all the places found on the page
     * @return List with a List of places
     */
    public static List<List<String>> placesFromUrl() {
        steder = new ArrayList<>();
        try {
            url = new URL("http://fil.nrk.no/yr/viktigestader/noreg.txt");

            /* This code is partly based on Oracles implementation
            * Source: https://docs.oracle.com/javase/tutorial/networking/urls/readingURL.html
            */
            br = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"));
            /* Skip first line */
            br.readLine();
            String random_string;
            while ((random_string = br.readLine()) != null) {
                steder.add(Arrays.asList(random_string.split("\t")));
            }

            br.close();

        } catch (MalformedURLException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        return steder;
    }

}
