package no.ntnu.imt3281.yr_places;

import java.util.ArrayList;
import java.util.List;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

public class Place {

    private String kommunenr, stedsnavn, prioritet, stedstype_nn, stedstype_nb, stedstype_en, kommune, fylke, moh, url_nn, url_nb, url_en;
    private float lat, lng;

    /**
     * Place constructor, creates places based on parameters given
     * @param data
     */
    public Place(List<String> data) {

        kommunenr = data.get(0);
        stedsnavn = data.get(1);
        prioritet = data.get(2);

        stedstype_nn = data.get(3);
        stedstype_nb = data.get(4);
        stedstype_en = data.get(5);

        kommune = data.get(6);
        fylke = data.get(7);
        lat = Float.parseFloat(data.get(8));
        lng = Float.parseFloat(data.get(9));
        moh = data.get(10);

        /* Check different urls */
        if(data.get(11).contains("Noreg") && data.get(11).contains("xml")) {
            url_nn = data.get(11);
        }
        if(data.get(12).contains("Norge") && data.get(12).contains("xml")) {
            url_nb = data.get(12);
        }
        /* Not all places have an english url. This checks for an english url */
        if(data.size() == 14) url_en = data.get(13);

    }

    /* Setters and getters related to the Place class. Setters are for the most part not used... */

    public int getKommunenr() {
        return Integer.parseInt(kommunenr);
    }

    public void setKommunenr(String kommunenr) {
        this.kommunenr = kommunenr;
    }

    public String getStedsnavn() {
        return stedsnavn;
    }

    public void setStedsnavn(String stedsnavn) {
        this.stedsnavn = stedsnavn;
    }

    public String getStedstype_nn() {
        return stedstype_nn;
    }

    public void setStedstype_nn(String stedstype_nn) {
        this.stedstype_nn = stedstype_nn;
    }

    public String getStedstype_nb() {
        return stedstype_nb;
    }

    public void setStedstype_nb(String stedstype_nb) {
        this.stedstype_nb = stedstype_nb;
    }

    public String getStedstype_en() {
        return stedstype_en;
    }

    public void setStedstype_en(String stedstype_en) {
        this.stedstype_en = stedstype_en;
    }

    public String getMoh() {
        return moh;
    }

    public void setMoh(String moh) {
        this.moh = moh;
    }

    public String getUrl_nn() {
        return url_nn;
    }

    public void setUrl_nn(String url_nn) {
        this.url_nn = url_nn;
    }

    public String getUrl_nb() {
        return url_nb;
    }

    public void setUrl_nb(String url_nb) {
        this.url_nb = url_nb;
    }

    public String getUrl_en() {
        return url_en;
    }

    public void setUrl_en(String url_en) {
        this.url_en = url_en;
    }

    public int getPrioritet() {
        return Integer.parseInt(prioritet);
    }

    public void setPrioritet(String prioritet) {
        this.prioritet = prioritet;
    }

    public String getStedstype() {
        return stedstype_nb;
    }

//    public void setStedstype(String stedstype) {
//        this.stedstype = stedstype;
//    }

    public String getKommune() {
        return kommune;
    }

    public void setKommune(String kommune) {
        this.kommune = kommune;
    }

    public String getFylke() {
        return fylke;
    }

    public void setFylke(String fylke) {
        this.fylke = fylke;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lang) {
        this.lng = lang;
    }

//    public void setVarselURL(String url) {
//        this.url = url;
//    }

    /**
     * Gets the belonging url, either in nynorsk or bokmal
     * @return
     */
    public String getVarselURL() {
        if(url_nb != null) return url_nb;
        else return url_nn;
    }

    public ArrayList<String> traverseXpathForecast() {
        ArrayList<String> content = new ArrayList<>();
        XPathFactory xpathFactory = XPathFactory.newInstance();

        return content;
    }
}
