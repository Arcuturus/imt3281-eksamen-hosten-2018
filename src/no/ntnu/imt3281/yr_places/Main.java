package no.ntnu.imt3281.yr_places;

import no.ntnu.imt3281.yr_db.DBAPI;

import java.sql.SQLException;

public class Main {

    private static DBAPI db;

    /**
     * Create a persistant DB file in the project
     * WARNING: Hangs 90% of the time...
     * @param args default args
     */
    public static void main(String[] args) {
        try {
            db = DBAPI.getPersistantInstance();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

    }

}
