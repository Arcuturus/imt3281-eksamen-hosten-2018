package no.ntnu.imt3281.yr_db;

import no.ntnu.imt3281.yr_places.Parser;
import no.ntnu.imt3281.yr_places.Place;

import java.sql.*;
import java.util.List;

public class DBAPI {

    static DBAPI dbapi = null;

    private static Connection con;
    private static Parser parser;
    private static List<List<String>> places;
    private static Place place;

    /**
     * DBAPI constructor, initializes a database
     * @param jdbc the current jdbc string argument
     * @throws SQLException
     */
    private DBAPI(String jdbc) throws SQLException {
        this.con = DriverManager.getConnection(jdbc);
        initializeDB();
    }

    /**
     * Temporary storage, mostly to use for testing. Removed when program finishes.
     * @return DBAPI object
     * @throws SQLException
     */
    public static DBAPI getMemoryInstance() throws SQLException {
        if(dbapi == null) {
            dbapi = new DBAPI("jdbc:derby:memory:yrDB;create=true");
            insertIntoDBFromUrl();
        }
        return dbapi;
    }

    /**
     * Persistant storage, saves a copy of the database as a file to the project
     * @return DBAPI object
     * @throws SQLException
     */
    public static DBAPI getPersistantInstance() throws SQLException {
        if(dbapi == null) {
            dbapi = new DBAPI("jdbc:derby:yrDB;create=true");
            insertIntoDBFromUrl();
        }
        return dbapi;
    }

    /**
     * Initialize the database with correct fields to be populated
     * @throws SQLException
     */
    private static void initializeDB() throws SQLException {
        String sql = "CREATE TABLE YrLocal ( " +
                "id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 0, INCREMENT BY 1), " +
                "kommunenr INTEGER NOT NULL, " +
                "stedsnavn VARCHAR(40) not null, " +
                "prioritet INTEGER NOT NULL, " +
                "stedstype_nn VARCHAR(50), " +
                "stedstype_nb VARCHAR(50), " +
                "stedstype_en VARCHAR(50), " +
                "kommune VARCHAR(20), " +
                "fylke VARCHAR(30), " +
                "latitude FLOAT NOT NULL, " +
                "longitude FLOAT NOT NULL, " +
                "moh VARCHAR(5), " +
                "url_nn VARCHAR(256), " +
                "url_nb VARCHAR(256), " +
                "url_en VARCHAR(256), " +
                "PRIMARY KEY (id))";
        Statement stmt = con.createStatement();
        stmt.execute(sql);
    }

    /**
     * Insert all elements read from the URL
     * @throws SQLException
     */
    public static void insertIntoDBFromUrl() throws SQLException {
        parser = new Parser();
        places = parser.placesFromUrl();
        /* Check if there already is a database, if not, create Place objects */
        if(getRowCount() < 1) {
            places.forEach(places -> {
                place = new Place(places);
                try {
                    addPlaceToDB(place);
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            });
        }
    }

    /**
     * Insert single element into database. Allows duplicates.
     * @param place Place object
     * @throws SQLException
     */
    public static void addPlaceToDB(Place place) throws SQLException {
        String stmt = "INSERT INTO YrLocal (" +
                "kommunenr, " +
                "stedsnavn, " +
                "prioritet, " +
                "stedstype_nn, " +
                "stedstype_nb, " +
                "stedstype_en, " +
                "kommune, " +
                "fylke, " +
                "latitude, " +
                "longitude, " +
                "moh, " +
                "url_nn, " +
                "url_nb, " +
                "url_en) VALUES (" +
                "?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        PreparedStatement pstmt = con.prepareStatement(stmt);

        pstmt.setInt(1, place.getKommunenr());
        pstmt.setString(2,place.getStedsnavn());
        pstmt.setInt(3, place.getPrioritet());
        pstmt.setString(4, place.getStedstype_nn());
        pstmt.setString(5, place.getStedstype_nb());
        pstmt.setString(6, place.getStedstype_en());
        pstmt.setString(7, place.getKommune());
        pstmt.setString(8, place.getFylke());
        pstmt.setFloat(9, place.getLat());
        pstmt.setFloat(10, place.getLng());
        pstmt.setString(11, place.getMoh());
        pstmt.setString(12, place.getUrl_nn());
        pstmt.setString(13, place.getUrl_nb());
        pstmt.setString(14, place.getUrl_en());

        pstmt.execute();

    }

    /**
     * Find a place in the database based on stedsnavn
     * @param stedsnavn the name of the place to check for
     * @return true if place is in databse, otherwise false
     * @throws SQLException
     */
    public static boolean findPlaceInDB(String stedsnavn) throws SQLException {
        String stmt =  "SELECT * FROM YRLOCAL WHERE stedsnavn=" + stedsnavn + ";";
        PreparedStatement pstmt = con.prepareStatement(stmt);
        ResultSet res = pstmt.executeQuery();
        return false;
    }

    /**
     * Finds the closest position to the provided x and y position
     * @param x the x position (longitude)
     * @param y the y position (latitude)
     * @return the name of the place closest to the point
     * @throws SQLException
     */
    public static String findPlaceByPosition(double x, double y) throws SQLException {
        double val, lat, lng, min = 9999, previous;
        int id = 0;
        String query = "SELECT * FROM YRLOCAL";
        PreparedStatement pstmt = con.prepareStatement(query);
        ResultSet res = pstmt.executeQuery();
        while(res.next()) {
            lat = Float.parseFloat(res.getString(10));
            lng = Float.parseFloat(res.getString(11));
            /* Pythagorean theorem to find the closest match */
            val = Math.sqrt(Math.pow((lat - x), 2) + Math.pow((lng - y), 2));
            if( val < min ) {
                min = val;
                /* Get the ID of the current closest element */
                id = res.getInt(1);
            }
        }
        /* After the closest element is found, query it by id so that I can return the name of the place */
        query = "SELECT * FROM YRLOCAL WHERE id=" + id;
        pstmt = con.prepareStatement(query);
        res = pstmt.executeQuery();
        res.next();
        return res.getString(3);
    }

    /**
     * Print all values in the database, mostly used for debugging purposes
     * @throws SQLException
     */
    public static void printAllDBValues() throws SQLException {
        String stmt = "SELECT * FROM YRLOCAL";
        PreparedStatement pstmt = con.prepareStatement(stmt);
        ResultSet res = pstmt.executeQuery();
        while(res.next()) {
            System.out.println(res.getString(2) + " : " +  res.getString(3));
        }
        res.close();
    }

    /**
     * Get the current number of rows in the database
     * @return integer, the number of rows
     * @throws SQLException
     */
    public static int getRowCount() throws SQLException {
        String stmt = "SELECT COUNT(*) FROM YRLOCAL";
        PreparedStatement pstmt = con.prepareStatement(stmt);
        ResultSet res = pstmt.executeQuery();
        res.next();
        return Integer.parseInt(res.getString(1));
    }

}
