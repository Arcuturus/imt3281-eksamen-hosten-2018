package no.ntnu.imt3281.click_areas;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.stage.Stage;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileWriter;
import java.io.IOException;

public class CreateClickAreas extends Application {

    @FXML TextField areaName;
    @FXML AnchorPane imageContainer;
    @FXML ListView points;
    @FXML TextArea areas;
    Scene scene;
    Pane temp_pane;
    JSONArray shapes;

    private ObservableList<Object> p = FXCollections.observableArrayList();
    Polygon polygon = null;

    public void initialize() {
        /* Don't want users to edit the area where the shapes are printed */
        areas.setEditable(false);
        shapes = new JSONArray();
    }

    /**
     * Build json data for shape
     */
    public void addArea() {
        polygon = null;
        temp_pane.getChildren().clear();
        for(Node node : temp_pane.getChildren()) {
            Polygon p = (Polygon) node;
            p.setFill(Color.grayRgb(0,0));
        }
        /* Require that the name of the shape is greater than 3 */
        if(areaName.getText().length() > 3) {
            /* Temporary JSONObject to append to the main JSONObject to avoid loss of data */
            JSONObject temp = new JSONObject().put("name", areaName.getText());
            JSONArray arr = new JSONArray();
            /* Iterate over, cast and append x and y position to the shapes polygon */
            for (Object obj : p) {
                Point point = (Point) obj;
                JSONObject jobj = new JSONObject();
                jobj.put("x", point.getX());
                jobj.put("y", point.getY());
                /* Append x and y position of polygon */
                arr.put(jobj);
            }
            temp.put("polygon", arr);
            shapes.put(temp);
            p.clear();
            areaName.clear();
            areas.setText(arr.toString());
        }
        /* Display alert to user if the name is too short */
        else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Save Error");
            alert.setHeaderText(null);
            alert.setContentText("Please give the shape a name!");
            alert.showAndWait();
        }
    }

    /* Save json to file */
    public void toFile() {
        if(!shapes.isEmpty()) {
            try {
                System.out.println(shapes.toString());
                FileWriter writer = new FileWriter("shapes.json");
                writer.write(shapes.toString());
                writer.flush();
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        else {
            System.out.println("There is nothing to save");
        }
    }

    /**
     * Finds the position of the mouse pointer and and creates polygon shapes based on the points clicked
     * @param me MouseEvent
     */
    @FXML
    public void findPos(MouseEvent me) {
        /* Get the mouse x and y position */
        Point point = new Point(me.getX(), me.getY());
        temp_pane = new Pane();
        System.out.println(point);
        p.add(point);
        /* Initiate a new polygon object if there are none available */
        if(polygon == null) {
            polygon = new Polygon();
            polygon.setStroke(Color.grayRgb(0, 0.9));
            polygon.setFill(Color.grayRgb(0, 0.2));
            /* An attempt to remove the polygons from the scene */
            temp_pane.setDisable(true);
            /* Append the polygon to the pane */
            temp_pane.getChildren().add(polygon);
        }
        /* Append points to polygon */
        polygon.getPoints().addAll(me.getX(), me.getY());
        /* Append the temp pane to the imageContainer */
        imageContainer.getChildren().add(temp_pane);
        points.setItems(p);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        BorderPane root = FXMLLoader.load(getClass().getResource("CreateClickAreas.fxml"));
        scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
