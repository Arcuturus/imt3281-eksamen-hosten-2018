package no.ntnu.imt3281.click_areas;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ClickAreas extends Application {

    @FXML Label areaToClick;
    @FXML TextArea results;

    private Scene scene;
    private JSONObject jobj;
    private JSONArray shapes;
    private int noOfShapesToGuess;
    private int currentGuessingValue;
    private int noOfCorrectGuesses;
    private ArrayList<Point> points;

    /* Initialize variables */
    public void initialize() throws IOException {
        currentGuessingValue = 0;
        noOfCorrectGuesses = 0;
        String jsondata = readFromFile();
        shapes = new JSONArray(jsondata);
        noOfShapesToGuess = shapes.length();
    }

    /**
     * Read the shapes from json file
     * Source: https://www.thepolyglotdeveloper.com/2015/03/parse-json-file-java/
     * @return the data from file as a String
     * @throws IOException
     */
    public String readFromFile() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("shapes.json"));
        StringBuilder sb = new StringBuilder();
        String line = br.readLine();
        while(line != null) {
            sb.append(line);
            line = br.readLine();
        }
        return sb.toString();
    }

    @FXML
    public void clicked(MouseEvent me) {
        currentPolygon(me.getX(), me.getY());
    }

    /**
     * Checks if the clicked section is of the correct shape type
     * @param x mouse X position
     * @param y mouse Y position
     */
    public void currentPolygon(double x, double y) {
        points = new ArrayList<>();
        /* Cast current shape to JSONObject */
        jobj = (JSONObject) shapes.get(currentGuessingValue);
        areaToClick.setText(jobj.get("name").toString());
        /* Cast the polygon section of the object to JSONArray for simpler iteration */
        JSONArray arr = (JSONArray) jobj.get("polygon");
        /* Iterate over all the points in the polygon and Create a Point object from each one */
        arr.forEach(polygon -> {
            JSONObject temp = (JSONObject) polygon;
            Point p = new Point(temp.getDouble("x"), temp.getDouble("y"));
            points.add(p);
        });
        /* As the Polygon class takes an array, convert ArrayList to array */
        Point[] ps = new Point[points.size()];
        points.toArray(ps);
        Polygon poly = new Polygon(ps);
        /* Create a point from the mouse x and y position */
        Point mousePoint = new Point(x, y);
        /* Check if the mouse point is contained within the polygon */
        if(poly.contains(mousePoint)) {
            results.appendText("Det er helt riktig!\n");
            currentGuessingValue++;
            noOfCorrectGuesses++;
            areaToClick.setText(jobj.get("name").toString());
        }
        else {
            results.appendText("Det var desverre feil, du skal klikke pa " + jobj.get("name") + "\n");
        }
        /* The user has guessed everything, display a message */
        if(noOfCorrectGuesses == noOfShapesToGuess) {
            results.appendText("Du har gjettet alt riktig!");
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        BorderPane root = FXMLLoader.load(getClass().getResource("ClickAreas.fxml"));
        scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

}
