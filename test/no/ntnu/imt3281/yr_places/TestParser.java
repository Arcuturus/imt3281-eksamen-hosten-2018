package no.ntnu.imt3281.yr_places;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestParser {

    @Test
    public void testParser() {
        //String data = "101	Asak kirke	55	Kyrkje	Kirke	Church	Halden	Østfold	59.14465	11.45458		http://www.yr.no/stad/Noreg/Østfold/Halden/Asak_kirke/varsel.xml	http://www.yr.no/sted/Norge/	";
        Parser parser = new Parser();
        Place p = new Place(parser.placesFromUrl().get(0));
        assertEquals(101, p.getKommunenr());
        assertEquals("Asak kirke", p.getStedsnavn());
        assertEquals("Kirke", p.getStedstype());
        assertEquals("Halden", p.getKommune());
        assertEquals("Østfold", p.getFylke());
        assertEquals(59.14465, p.getLat(), 0.00001);
        assertEquals(11.45458, p.getLng(), 0.00001);
        assertEquals("http://www.yr.no/stad/Noreg/Østfold/Halden/Asak_kirke/varsel.xml", p.getVarselURL());		// Varsel finnes ikke på bokmål, returner på nynorsk

    }

}
