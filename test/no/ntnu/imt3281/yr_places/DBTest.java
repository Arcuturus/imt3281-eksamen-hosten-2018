package no.ntnu.imt3281.yr_places;

import no.ntnu.imt3281.yr_db.DBAPI;
import org.junit.Test;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.Arrays;

public class DBTest {

    DBAPI db;

    @Test
    public void testDBConnection() {
        boolean exceptionThrown = false;
        try {
            db = DBAPI.getMemoryInstance();
            assertEquals(db.getRowCount(), 10995, 0.0);

            String data = "101	Asak kirke	55	Kyrkje	Kirke	Church	Halden	Østfold	59.14465	11.45458		http://www.yr.no/stad/Noreg/Østfold/Halden/Asak_kirke/varsel.xml	http://www.yr.no/sted/Norge/	";
            Place p = new Place(Arrays.asList(data.split("\t")));
            db.addPlaceToDB(p);
            assertEquals(db.getRowCount(), 10996, 0.0);
            assertEquals(db.findPlaceByPosition(59.14465, 11.4545), "Asak kirke");

        } catch (SQLException ex) {
            exceptionThrown = true;
            System.out.println(ex.getMessage());
        }
        assertEquals(exceptionThrown, false);

    }

}
